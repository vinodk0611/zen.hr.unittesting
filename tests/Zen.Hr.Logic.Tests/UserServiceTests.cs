﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Zen.Hr.Logic.Tests
{
    [TestClass]
    public class UserServiceTests
    {
        private UserService _userService;
        private Mock<IUserDataAccess> _userData; // dependency of controller

        [TestInitialize]
        ///GetAllUsers is set up to return 10 users using Mocking and Active Users of 5
        public void Initialize()
        {
            _userData = new Mock<IUserDataAccess>();
            _userService = new UserService(_userData.Object);
            _userData.Setup(x => x.GetAllUsers()).Returns(new User[10]);
            _userData.Setup(x => x.GetAllActiveUsers()).Returns(new User[5]);
        }

        [TestMethod]        
        public void Successfully_Returns_All_Users_When_Passing_In_False()
        {
            var expected = new User[10];

            // unit test goes here
            var list = _userService.GetUsers(false);
            CollectionAssert.AreEqual(expected, list);
        }

        [TestMethod]
        public void Successfully_Returns_All_Active_Users_When_Passing_In_True()
        {
            var expected = new User[5];
            
            // unit test goes here
            var list = _userService.GetUsers(true);
            CollectionAssert.AreEqual(expected, list);
        }
    }
}
